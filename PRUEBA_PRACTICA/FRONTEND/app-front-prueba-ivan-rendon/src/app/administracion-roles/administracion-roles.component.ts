import { Component, OnInit } from '@angular/core';
import { AppService } from '../servicios/app.service';
import { MatTableDataSource } from '@angular/material/table';

import { MatDialog } from '@angular/material/dialog';
import { AgregarRolModalComponent } from '../componentes/agregar-rol-modal/agregar-rol-modal.component';
import { ModificarRolModalComponent } from '../componentes/modificar-rol-modal/modificar-rol-modal.component';

@Component({
  selector: 'app-administracion-roles',
  templateUrl: './administracion-roles.component.html',
  styleUrls: ['./administracion-roles.component.css']
})
export class AdministracionRolesComponent implements OnInit{
  
  roles: any = [ ];

  columnas: string[] = ['id', 'nombre', 'descripcion', 'acciones'];

  datos = new MatTableDataSource<any>(this.roles);

  filtro : string = '';

  constructor(private appService : AppService, public dialog:MatDialog) { }
  
  ngOnInit(): void {
    this.appService.getRoles().subscribe( 
      (data) => {
        console.log(data);
        this.roles = data;
        this.datos = new MatTableDataSource<any>(this.roles);
      }

    );
  }

  editarRol(row : any){
    if(row.nombre == 'ADMINISTRADOR'){
      alert("No se puede editar el rol ADMINISTRADOR");
      return;
    }

    const dialogRef = this.dialog.open(ModificarRolModalComponent, {
      width: '400px', // Configura el ancho del modal
      data: { id: row.id, nombre: row.nombre, descripcion: row.descripcion }
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result === true ){
        alert("Rol modificado correctamente");
      }
      this.ngOnInit()
    });
    
  }

  eliminarRol(row : any){
    console.log(row);

    if(row.nombre == 'ADMINISTRADOR'){
      alert("No se puede eliminar el rol ADMINISTRADOR");
      return;
    }

    this.appService.borrarRol(row.nombre).subscribe(
      (data) => {
        console.log(data);
        this.ngOnInit();
        if(data == true){
          alert("Rol eliminado correctamente");
        }else{
          alert("No se pudo eliminar el rol");
        }

      }
    );
  }

  aplicarFiltro(event : any){
    this.filtro = event.target.value;
    this.datos.filter = this.filtro.toLowerCase().trim();
  }

  agregarRol(){
    const dialogRef = this.dialog.open(AgregarRolModalComponent, {
      width: '400px', // Configura el ancho del modal
      data: { /* Datos que deseas pasar al modal */ }
    });

    dialogRef.afterClosed().subscribe(result => {
      alert("Rol agregado correctamente");
      this.ngOnInit()
    });
  }

}
