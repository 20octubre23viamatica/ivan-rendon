import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { API_URL } from '../constantes/ApiUrl';

@Injectable({
    providedIn: 'root'
})
export class AppService {
    constructor(private http: HttpClient) { }

    getRoles() {
        return this.http.get(`${API_URL}/rol`);
    }

    borrarRol(nombre: any){
        return this.http.delete(`${API_URL}/rol/${nombre}`);
    }

    guardarRol(nombre: string, descripcion: string){
        return this.http.post(`${API_URL}/rol`, { nombre, descripcion });
    }

    modificarRol(id: number, nombre: string, descripcion: string){
        return this.http.put(`${API_URL}/rol`, { id, nombre, descripcion });
    }
}
