import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { API_URL } from '../constantes/ApiUrl';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    logueado : boolean = this.estaLogueado();

    constructor(private http: HttpClient) {}

    login(datosLogin: any) {
        return this.http.post(`${API_URL}/auth/login`, datosLogin);
    }

    logout() {
        localStorage.removeItem('usuario');
        this.logueado = this.estaLogueado();
    }

    register() {
        
    }

    estaLogueado() {
        if(localStorage.getItem('usuario') == null || localStorage.getItem('usuario') == undefined){
            return false;
        }else{
            return true;
        }
    }

    obtenerMiRol(){
        let usuario = JSON.parse(localStorage.getItem('usuario') || '{}');
        console.log(usuario);
        return this.http.post(`${API_URL}/auth/mirol`, usuario);
    }

}
