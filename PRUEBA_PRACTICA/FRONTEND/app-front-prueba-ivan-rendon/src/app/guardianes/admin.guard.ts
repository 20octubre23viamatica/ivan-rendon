import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../servicios/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private router: Router, private authService:AuthService) {}

  canActivate(): boolean {
    if( this.authService.obtenerMiRol().subscribe(rol => { 
      rol == 'ADMINISTRADOR' ? true : false;  
    })){

      return true;
    }else{
      return false;
    }
  }


}