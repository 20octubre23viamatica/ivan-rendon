import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../servicios/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm: FormGroup = this.formBuilder.group({
    usuario: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(private formBuilder: FormBuilder, private authService: AuthService) {
  }


  login() {
    this.authService.login(this.loginForm.value).subscribe(
      (data:any) => {
        console.log(data);
        if(data['id'] !== null && data['id'] !== undefined){
          localStorage.setItem('usuario', JSON.stringify(data));
          window.location.href = '/dashboard';
        }
      }

    );
  }
}
