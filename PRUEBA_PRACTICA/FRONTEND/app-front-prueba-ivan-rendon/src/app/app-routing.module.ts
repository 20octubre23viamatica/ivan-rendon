import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdministracionUsuariosComponent } from './administracion-usuarios/administracion-usuarios.component';
import { AdministracionRolesComponent } from './administracion-roles/administracion-roles.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './guardianes/auth.guard';
import { AdminGuard } from './guardianes/admin.guard';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'usuarios',
    component: AdministracionUsuariosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'roles',
    component: AdministracionRolesComponent,
    canActivate: [AdminGuard]
  },
  {
    path: '**',
    redirectTo: 'login'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
