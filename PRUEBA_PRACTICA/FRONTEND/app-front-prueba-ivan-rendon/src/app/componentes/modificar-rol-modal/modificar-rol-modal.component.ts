import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AppService } from 'src/app/servicios/app.service';

@Component({
  selector: 'app-modificar-rol-modal',
  templateUrl: './modificar-rol-modal.component.html',
  styleUrls: ['./modificar-rol-modal.component.css']
})
export class ModificarRolModalComponent implements OnInit{

  nombreRol: string = '';
  descripcionRol: string = '';
  id : number = 0;

  constructor(private appService: AppService, 
    public dialogRef: MatDialogRef<ModificarRolModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any){}


  ngOnInit(): void {
    this.nombreRol = this.data.nombre;
    this.descripcionRol = this.data.descripcion;
    this.id = this.data.id;
  }

  guardar(){
    this.appService.modificarRol(this.id ,this.nombreRol, this.descripcionRol).subscribe(
      (data:any) => {
        return data;
      }
    );

    this.dialogRef.close(); 

  }
}
