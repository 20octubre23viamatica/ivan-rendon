import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AppService } from 'src/app/servicios/app.service';

@Component({
  selector: 'app-agregar-rol-modal',
  templateUrl: './agregar-rol-modal.component.html',
  styleUrls: ['./agregar-rol-modal.component.css']
})
export class AgregarRolModalComponent {

  nombreRol: string = '';
  descripcionRol: string = '';

  constructor(private appService: AppService, public dialogRef: MatDialogRef<AgregarRolModalComponent>){}

  guardar(){
    this.appService.guardarRol(this.nombreRol, this.descripcionRol).subscribe(
      (data:any) => {
        return data;
      }
    );

    this.dialogRef.close(); 

  }

}
