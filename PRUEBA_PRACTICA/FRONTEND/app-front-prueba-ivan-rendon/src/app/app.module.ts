import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './menu/menu.component';
import { AdministracionUsuariosComponent } from './administracion-usuarios/administracion-usuarios.component';
import { AdministracionRolesComponent } from './administracion-roles/administracion-roles.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { LoginComponent } from './login/login.component';
import { AuthService } from './servicios/auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { AppService } from './servicios/app.service';

import { MatIconModule } from '@angular/material/icon';
import { AgregarRolModalComponent } from './componentes/agregar-rol-modal/agregar-rol-modal.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ModificarRolModalComponent } from './componentes/modificar-rol-modal/modificar-rol-modal.component';




@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    AdministracionUsuariosComponent,
    AdministracionRolesComponent,
    LoginComponent,
    AgregarRolModalComponent,
    ModificarRolModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule, 
    MatSortModule, 
    MatFormFieldModule, 
    MatInputModule, 
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatDialogModule 
  ],
  providers: [AuthService, AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
