package ec.telconet.prueba.ivan.rendon.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.telconet.prueba.ivan.rendon.model.AdmiRol;
import ec.telconet.prueba.ivan.rendon.service.RolService;

@RestController
@RequestMapping("/rol")
@CrossOrigin("*")
public class RolesController {
    
    @Autowired
    private RolService rolService;

    @GetMapping()
    public ResponseEntity<List<AdmiRol>> obtenerRoles(){
        try{
            return ResponseEntity.ok(rolService.obtenerRoles());
        }catch(Exception e){
            return ResponseEntity.badRequest().body(null);
        }
    }

    @DeleteMapping("/{nombreRol}")
    public ResponseEntity<Boolean> borrarRol(@PathVariable String nombreRol){
        try{
            System.out.println("El nombreee: " + nombreRol);
            return ResponseEntity.ok(rolService.borrarRol(nombreRol));
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PostMapping()
    public ResponseEntity<Boolean> guardarRol(@RequestBody AdmiRol rol){
        try{
            return ResponseEntity.ok(rolService.guardarRol(rol));
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PutMapping()
    public ResponseEntity<Boolean> actualizarRol(@RequestBody AdmiRol rol){
        try{
            return ResponseEntity.ok(rolService.actualizarRol(rol));
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().body(null);
        }
    }

}
