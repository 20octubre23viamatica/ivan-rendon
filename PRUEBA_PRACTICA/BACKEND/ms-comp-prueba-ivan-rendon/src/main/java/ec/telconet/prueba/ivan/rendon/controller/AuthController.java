package ec.telconet.prueba.ivan.rendon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.telconet.prueba.ivan.rendon.dto.LoginRequest;
import ec.telconet.prueba.ivan.rendon.model.AdmiRol;
import ec.telconet.prueba.ivan.rendon.model.AdmiUsuario;
import ec.telconet.prueba.ivan.rendon.service.AuthService;

@RestController
@RequestMapping("/auth")
@CrossOrigin("*")
public class AuthController {
    
    @Autowired
    private AuthService authService;    

    @PostMapping("/login")
    public ResponseEntity<AdmiUsuario> login(@RequestBody LoginRequest loginRequest ) {
        
        AdmiUsuario usuario = authService.obtenerUsuario(loginRequest.getUsuario(), loginRequest.getPassword());    

        if(usuario != null) {
            return ResponseEntity.ok(usuario);
        } else {
            return ResponseEntity.badRequest().body(null);
        }

    }


    @PostMapping("/mirol")
    public ResponseEntity<AdmiRol> obtenerMiRol(@RequestBody AdmiUsuario usuario){
        try{
            System.out.println("Usuario: " + usuario);
            return ResponseEntity.ok(authService.obtenerRolDeUsuario(usuario));
        }catch(Exception e){
            return ResponseEntity.badRequest().body(null);
        }
    }


    

}
