package ec.telconet.prueba.ivan.rendon.dto;

import java.security.Timestamp;

import ec.telconet.prueba.ivan.rendon.model.AdmiRol;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegistrarUsuarioRequest {
    
    private Long id;

    private String usuario;

    private String password;

    private String nombres;

    private String apellidos;

    private String direccion;

    private String telefono;

    private String usuarioCreacion;

    private Timestamp fechaCreacion;

    private String foto;

    private AdmiRol rol;

}
