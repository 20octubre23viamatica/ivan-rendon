package ec.telconet.prueba.ivan.rendon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.telconet.prueba.ivan.rendon.model.AdmiRol;
import ec.telconet.prueba.ivan.rendon.model.AdmiUsuario;
import ec.telconet.prueba.ivan.rendon.model.InfoUsuarioRol;
import java.util.List;


@Repository
public interface InfoUsuarioRolRepository extends JpaRepository<InfoUsuarioRol, Long> {
    
    List<InfoUsuarioRol> findByAdmiUsuario(AdmiUsuario admiUsuario);

    List<InfoUsuarioRol> findByAdmiRol(AdmiRol admiRol);

}
