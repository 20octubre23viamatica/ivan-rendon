package ec.telconet.prueba.ivan.rendon.model;



import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "admi_usuaurio")
public class AdmiUsuario {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

    private String usuario;

    private String password;

    private String nombres;

    private String apellidos;

    private String direccion;

    private String telefono;

    private String usuarioCreacion;

    private Timestamp fechaCreacion;

    private String foto;

}
