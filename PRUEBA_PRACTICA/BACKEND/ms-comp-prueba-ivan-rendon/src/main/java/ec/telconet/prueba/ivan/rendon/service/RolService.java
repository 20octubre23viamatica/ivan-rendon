package ec.telconet.prueba.ivan.rendon.service;

import java.util.List;

import javax.sound.midi.MidiDevice.Info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.telconet.prueba.ivan.rendon.model.AdmiRol;
import ec.telconet.prueba.ivan.rendon.model.InfoUsuarioRol;
import ec.telconet.prueba.ivan.rendon.repository.AdmiRolRepository;
import ec.telconet.prueba.ivan.rendon.repository.InfoUsuarioRolRepository;

@Service
public class RolService {
    
    @Autowired
    private AdmiRolRepository admiRolRepository;    

    @Autowired
    private InfoUsuarioRolRepository infoUsuarioRolRepository;    

    public boolean existeRol(String nombreRol){
        return admiRolRepository.findByNombre(nombreRol).isPresent();
    }

    public boolean guardarRol(AdmiRol rol){
        try{
            admiRolRepository.save(rol);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public AdmiRol obtenerRolAdministrador(){
        return admiRolRepository.findByNombre("ADMINISTRADOR").get();
    }

    public AdmiRol obtenerRol(String nombreRol){
        return admiRolRepository.findByNombre(nombreRol).get();
    }    

    public List<AdmiRol> obtenerRoles(){
        return admiRolRepository.findAll();
    }


    public boolean borrarRol(String nombreRol){
        try{
            AdmiRol rol = admiRolRepository.findByNombre(nombreRol).get();
            if(rol.getNombre().equals("ADMINISTRADOR")){
                return false;
            }

            //Primero borrar todos los roles de Info
            List<InfoUsuarioRol> infoUsuarioRoles = infoUsuarioRolRepository.findByAdmiRol(rol);

            for(InfoUsuarioRol infoUsuarioRol : infoUsuarioRoles){
                infoUsuarioRol.setAdmiRol(null);
            }

            infoUsuarioRolRepository.saveAll(infoUsuarioRoles);

            admiRolRepository.delete(rol);
            return true;
        }catch(Exception e){
            return false;
        }

    }

    public boolean actualizarRol(AdmiRol rol) {
       try{
            admiRolRepository.save(rol);
            return true;
        }catch(Exception e){
            return false;
        }
    }

}
