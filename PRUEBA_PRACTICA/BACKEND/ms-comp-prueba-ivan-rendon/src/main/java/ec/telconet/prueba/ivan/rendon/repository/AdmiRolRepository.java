package ec.telconet.prueba.ivan.rendon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.telconet.prueba.ivan.rendon.model.AdmiRol;
import java.util.Optional;


@Repository
public interface AdmiRolRepository extends JpaRepository<AdmiRol, Long>{
    
    Optional<AdmiRol> findByNombre(String nombre);

    
    

}
