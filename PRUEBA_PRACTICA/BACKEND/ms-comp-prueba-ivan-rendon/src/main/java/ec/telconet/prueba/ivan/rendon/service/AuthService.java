package ec.telconet.prueba.ivan.rendon.service;

import java.sql.Timestamp;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.qos.logback.core.encoder.EchoEncoder;
import ec.telconet.prueba.ivan.rendon.dto.RegistrarUsuarioRequest;
import ec.telconet.prueba.ivan.rendon.model.AdmiRol;
import ec.telconet.prueba.ivan.rendon.model.AdmiUsuario;
import ec.telconet.prueba.ivan.rendon.model.InfoUsuarioRol;
import ec.telconet.prueba.ivan.rendon.repository.AdmiUsuarioRepository;
import ec.telconet.prueba.ivan.rendon.repository.InfoUsuarioRolRepository;

@Service
public class AuthService {
    
    @Autowired
    private AdmiUsuarioRepository admiUsuarioRepository;

    @Autowired
    private InfoUsuarioRolRepository infoUsuarioRolRepository;

    @Autowired 
    private RolService rolService;

    public AdmiUsuario obtenerUsuario(String usuario, String password) {
        Optional<AdmiUsuario> usuarioOptional = admiUsuarioRepository.findByUsuarioAndPassword(usuario, password);  

        if(usuarioOptional.isPresent()) {
            return usuarioOptional.get();
        } else {
            return null;
        }
        
    }


    public AdmiUsuario registrarUsuario(RegistrarUsuarioRequest registrarUsuarioRequest){

        if(rolService.existeRol(registrarUsuarioRequest.getRol().getNombre()) == false){
            return null;
        }

        if(existeNombreUsuario(registrarUsuarioRequest.getUsuario())){
            return null;
        }

        AdmiUsuario admiUsuario = new AdmiUsuario();
        admiUsuario.setApellidos(registrarUsuarioRequest.getApellidos());
        admiUsuario.setUsuario(registrarUsuarioRequest.getUsuario());
        admiUsuario.setPassword(registrarUsuarioRequest.getPassword());
        admiUsuario.setTelefono(registrarUsuarioRequest.getTelefono());
        admiUsuario.setDireccion(registrarUsuarioRequest.getDireccion());
        admiUsuario.setFoto(registrarUsuarioRequest.getFoto());

        AdmiUsuario usaurioGuardado = admiUsuarioRepository.save(admiUsuario);

        InfoUsuarioRol infoUsuarioRol = new InfoUsuarioRol();
        infoUsuarioRol.setAdmiUsuario(usaurioGuardado);
        infoUsuarioRol.setAdmiRol(rolService.obtenerRol(registrarUsuarioRequest.getRol().getNombre()) );
        infoUsuarioRol.setEstado("ACTIVO");
        infoUsuarioRol.setUsuarioCreacion(registrarUsuarioRequest.getUsuarioCreacion());
        infoUsuarioRol.setFechaCreacion(new Timestamp(System.currentTimeMillis()));
        

        infoUsuarioRolRepository.save(infoUsuarioRol);

        return usaurioGuardado;
    }

    public boolean existeNombreUsuario(String usuario){
        return admiUsuarioRepository.findByUsuario(usuario).isPresent();
    }


    public AdmiRol obtenerRolDeUsuario(AdmiUsuario usuario){
        try{

            return infoUsuarioRolRepository.findByAdmiUsuario(usuario).get(0).getAdmiRol();
        }catch(Exception e){
            return null;
        }
    }
}
