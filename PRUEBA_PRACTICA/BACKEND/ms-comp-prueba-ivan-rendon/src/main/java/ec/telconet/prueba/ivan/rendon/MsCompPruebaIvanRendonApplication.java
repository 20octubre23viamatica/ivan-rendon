package ec.telconet.prueba.ivan.rendon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ec.telconet.prueba.ivan.rendon.dto.RegistrarUsuarioRequest;
import ec.telconet.prueba.ivan.rendon.model.AdmiRol;
import ec.telconet.prueba.ivan.rendon.service.AuthService;
import ec.telconet.prueba.ivan.rendon.service.RolService;

@SpringBootApplication
public class MsCompPruebaIvanRendonApplication implements CommandLineRunner{

	@Autowired
	private AuthService authService;

	@Autowired
	private RolService rolService;
	

	public static void main(String[] args) {
		SpringApplication.run(MsCompPruebaIvanRendonApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//Crear roles
		AdmiRol administrador = new AdmiRol();
		administrador.setNombre("ADMINISTRADOR");

		AdmiRol editor = new AdmiRol();
		editor.setNombre("EDITOR");

		AdmiRol consultor = new AdmiRol();
		consultor.setNombre("CONSULTOR");

		// rolService.guardarRol(administrador);
		// rolService.guardarRol(editor);
		// rolService.guardarRol(consultor);
		
		//Crear usuario administrador

		RegistrarUsuarioRequest registrarUsuarioRequest = new RegistrarUsuarioRequest();
		registrarUsuarioRequest.setUsuario("administrador@prueba.com");
		registrarUsuarioRequest.setPassword("administrador");
		registrarUsuarioRequest.setNombres("Administrador");
		registrarUsuarioRequest.setRol( AdmiRol.builder().nombre("ADMINISTRADOR").build() );
		

		authService.registrarUsuario(registrarUsuarioRequest);

	}

	

}
