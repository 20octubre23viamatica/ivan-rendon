package ec.telconet.prueba.ivan.rendon.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.telconet.prueba.ivan.rendon.model.AdmiUsuario;

@Repository
public interface AdmiUsuarioRepository extends JpaRepository<AdmiUsuario, Long>{
    
    Optional<AdmiUsuario> findByUsuarioAndPassword(String usuario, String password);

    Optional<AdmiUsuario> findByUsuario(String usuario);
}
