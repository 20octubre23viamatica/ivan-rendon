package com.rendonivan.algoritmo1;


public class Main {
    public static void main(String[] args) {
    
        System.out.println("*****************PRUEBA 1***********************");
        System.out.println("Prueba con: [1, -1, -1, -2, 1, -2, -1 , -2, 1, 2]");
        int[] prueba1 = {1, -1, -1, -2, 1, -2, -1 , -2, 1, 2};
        algoritmo2(prueba1);

        System.out.println("*****************PRUEBA 2***********************");
        System.out.println("Prueba con: [1, 0, 0, -1, 0, 0, 1, 1]");
        int[] prueba2 = {1, 0, 0, -1, 0, 0, 1, 1} ;
        algoritmo2(prueba2);

        System.out.println("*****************EJEMPLO DE LA PRUEBA***********************");
        System.out.println("Prueba con: [1, 0, 0, 0]");
        int[] prueba3 = { 1, 0, 0, 0};
        algoritmo2(prueba3);
    }

    

    public static void algoritmo2(int[] numsArray) {
        int posX = 0;
        int posY = 0;

        for(int i = 0; i < numsArray.length; i+=2){
            int movimientoX = numsArray[i];
            int movimientoY = numsArray[i+1];

            //Si movimiento X es positivo se mueve a la derecha
            if(movimientoX > 0){
                posX += movimientoX;
            }else {
                posX -= movimientoX;
            }

            //Validar que no se salga del mapa
            if(posX > 3){
                    posX = 3;
            }else if(posX < 0){
                posX = 0;
            }
        
            //Si movimiento Y es positivo se mueve abajo
            if( movimientoY > 0){
                posY -= movimientoY;
            }else{
                posY += movimientoY;
            }

            //Validar que no se salga del mapa
            if(posY > 3){
                    posY = 3;
            }else if(posY < 0){
                posY = 0;
            }

        }

        System.out.println("posicion x: " + posX + ", posicion y: " + posY );

        //Imprimir el mapa
        for(int y = 0; y < 4; y++){
            for(int x = 0; x < 4; x++){
                if(x == posX && y == posY){
                    System.out.print("X");
                }else{
                    System.out.print("0");
                }
            }
            System.out.println();
        }
        
    }
    

}