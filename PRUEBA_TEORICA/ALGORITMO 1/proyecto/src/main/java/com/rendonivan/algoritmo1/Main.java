package com.rendonivan.algoritmo1;


public class Main {
    public static void main(String[] args) {
    
        System.out.println("*****************PRUEBA 1***********************");
        System.out.println("Prueba con: [1, 2, 3, 4, 5, 7, 7 ,7, 8, 8]");
        System.out.println("Aquí deberia ser numero 7 y recurrencias 3, ya que es el que mas se repite");
        int[] prueba1 = {1, 2, 3, 4, 5, 7, 7 ,7, 8, 8};
        algoritmo1(prueba1);

        System.out.println("*****************PRUEBA 2***********************");
        System.out.println("Aquí se repite el 5 y el 7 la misma cantidad de veces, pero debe salir el numero 5 porque es el primero que aparece de izq. a der.");
        System.out.println("Prueba con: [1, 2, 3, 4, 5, 5, 5, 7, 7 ,7]");
        int[] prueba2 = {1, 2, 3, 4, 5, 5, 5, 7, 7 ,7};
        algoritmo1(prueba2);

        System.out.println("*****************EJEMPLO DE LA PRUEBA***********************");
         System.out.println("Aquí deberia ser numero 8 y recurrencias 3, ya que es el que mas se repite");
        System.out.println("Prueba con: [1, 2, 2, 5, 4, 6, 7, 8, 8, 8 ]");
        int[] prueba3 = { 1, 2, 2, 5, 4, 6, 7, 8, 8, 8 };
        algoritmo1(prueba3);
    }

    

    public static void algoritmo1(int[] numsArray) {
        
        int numeroQueMasSeRepite = 0;
        int vecesQueSeRepite = 0;

        int numeroActual = 0;
        int vecesQueSeRepiteActual = 0;

        //Se va escogiendo por orden un numero del array
        for (int i = 0; i < numsArray.length; i++){
            numeroActual = i;
            //Luego se recorre el array para ver cuantas veces se repite
            for(int j = 0; j < numsArray.length; j ++){
                //Si el numero es igual al numero actual, se suma 1 a la variable vecesQueSeRepiteActual
                if (numsArray[j] == numeroActual){
                    vecesQueSeRepiteActual++;
                }
            }

            //Si el mayor numero de veces que se repite actual es mayor al mayor numero de veces que se repite, se cambia el numero que mas se repite
            //Al ser mayor y no mayor igual, se queda el primero que se repita mas veces, entonces si hay 2 numeros 
            //que se repiten la misma cantidad de veces, se queda el primero
            if(vecesQueSeRepiteActual > vecesQueSeRepite){
                numeroQueMasSeRepite = numeroActual;
                vecesQueSeRepite = vecesQueSeRepiteActual;
            }

            //Se formatea a 0 para empezar de nuevo a contar
            vecesQueSeRepiteActual = 0;
        }

        System.out.println("Recurrencias: " + vecesQueSeRepite);
        System.out.println("Número: " + numeroQueMasSeRepite);
        
    }
    

}